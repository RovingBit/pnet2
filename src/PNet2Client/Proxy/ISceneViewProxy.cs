﻿namespace PNetC
{
    public interface ISceneViewProxy
    {
        NetworkedSceneView View { get; set; }
    }
}
